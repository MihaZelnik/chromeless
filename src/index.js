const setup = require('./starter-kit/setup');

exports.handler = async (event, context, callback) => {
  // For keeping the browser launch
  context.callbackWaitsForEmptyEventLoop = false;
  const queryStringParameters = event.queryStringParameters;
  const {url} = queryStringParameters;
  console.log('Fetching data');
  const browser = await setup.getBrowser();
  console.log(url);
  try {
    const result = await exports.run(browser, url);
    callback(null, {
      statusCode: 200,
      body: result,
      headers: {
        'Content-Type': 'text/html',
      },
    });
  } catch (e) {
    callback(e);
  }
};

exports.run = async (browser, url) => {
  const page = await browser.newPage();
  await page.goto(url);
  let content = await page.content();
  console.log('Done with content');
  return content;
};
